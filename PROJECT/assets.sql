-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 11, 2020 at 02:52 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `isms`
--

-- --------------------------------------------------------

--
-- Table structure for table `assets`
--

CREATE TABLE `assets` (
  `Name` varchar(100) DEFAULT NULL,
  `Location` varchar(100) DEFAULT NULL,
  `Maincode` int(50) DEFAULT NULL,
  `subcode` int(50) DEFAULT NULL,
  `PurchaseYear` int(50) DEFAULT NULL,
  `Qty` int(255) DEFAULT NULL,
  `Type` varchar(50) DEFAULT NULL,
  `Domain` varchar(150) NOT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacture` varchar(255) DEFAULT NULL,
  `OS` varchar(255) DEFAULT NULL,
  `Processor` varchar(255) DEFAULT NULL,
  `Harddisk` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assets`
--

INSERT INTO `assets` (`Name`, `Location`, `Maincode`, `subcode`, `PurchaseYear`, `Qty`, `Type`, `Domain`, `Model`, `Manufacture`, `OS`, `Processor`, `Harddisk`) VALUES
('Cpu', 'lab1', 11, 22, 2017, 11, 'physical', 'lab1.com', 'sc-111', 'intel', 'windows 10', 'intel', '1 TB');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
