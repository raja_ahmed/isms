



<!DOCTYPE html>
<html>

<head>
  <meta charset='UTF-8'>

  <title>Register Assets</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"> 
  <link rel='stylesheet' href='assets/css/style.css'>
  <link rel='stylesheet' href='assets/css/form.css'>
  

</head>

<body>

<!--    <div id="page-wrap" class="group">  -->

     <img src="assets/img/isms_banner.jpg" alt="assets/img/isms_banner.png"  class="responsive" / > 

    
   

    <div class="header-right">

      <nav class="navbar navbar-inverse">
  <div class="container-fluid">
  
    <div class="navbar-header">
      <a class="navbar-brand" href="index.html"><img src="assets/img/main logo.png"></a>

    </div>
    
       <ul class="nav navbar-nav navbar-right" class="margin">
      <li><a href="login.html"><span class="glyphicon glyphicon-user"></span> Login</a></li>
      <li><a href="signup.html"><span class="glyphicon glyphicon-user"></span> Sign UP <em>(new member)</em></a></li>
      </ul>
      
     </nav>
    </div>
    
    </div>

<!-- The sidebar -->
<div class="sidebar">
  <a  href="addcpu.php">Add CPU</a>
  <a href="addprinter.php">Add Printer</a>
  <a href="addscanner.php">Add Scanner</a>
  <a href="addlaptop.php">Add Laptop</a>
  <a href="addserver.php">Add Server</a>
  <a href="addlcd.php">Add LCD</a>
  <a href="addmultimedia.php">Add Multimedia</a>
  <a href="addrouter.php">Add Router</a>
   <a href="addnetworkstore.php">Add Network Storage</a>
  
  
</div>

    <div class="content">
<div class="bg"> 


<div class="form">
    <form action="" method="post" >
    <h2 class="text-center">Register Multi-Media</h2><hr> 
    <div class="form-group col-sm-4">
   <label>Asset Name</label>  <input type="text" class="form-control"  name="name" required="required"></div>


    <div class="form-group col-sm-4">
   <label>Location</label>  <input type="text" class="form-control"  name="loc" required="required"></div>

   <div class="form-group col-sm-4">
      <label>Main Code:</label> <input type="text"  class="form-control" name="maincode" required="required"></br></div>

      <div class="form-group col-sm-4" >
     <label> Sub-Code:</label> <input type="text" class="form-control" name="subcode" required="required"></br></div>

     <div class="form-group col-sm-4">
      <label>Purchase Year:</label> <input type="text" class="form-control" name="pyear" required="required"></br></div>

      <div class="form-group col-sm-4 ">
      <label>Quantity:</label> <input type="text" class="form-control" name="qty" required="required"></br></div>

      <div class="form-group col-sm-6">
      <label>Type:</label> <input type="text" class="form-control" name="type" required="required"></br></div>

      <div class="form-group col-sm-6">
      <label>Domain:</label> <input type="text" class="form-control" name="domain" required="required"></br></div>

      <div class="form-group col-sm-6">
      <label>Model:</label> <input type="text" class="form-control" name="model" required="required"></br></div>

      <div class="form-group col-sm-6">
      <label>Manufacture:</label> <input type="text" class="form-control" name="make" required="required"></br></div><hr>

   <div class="form-group" >
            <button type="submit" name="Submit" class="btn btn-primary btn-block" <?php require_once "form.php";?> >Submit</button>
        </div>


  
      

    </form>
</div>
</div>
    
</div>

<footer>
   <div class="footer">
   <img src="assets/img/logo.png" height="150px"><br>
          
          © Copyright 2019  
          <a href="index.html" style="color: grey;">  UIIT ISMS</a>
     
   </div>
  </footer>

</body>

</html>